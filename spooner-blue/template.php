<?php

function phptemplate_boxClass($sidebar_left = "", $sidebar_right = "") {
  if ($sidebar_left && $sidebar_right) {
    $class = "boxBoth";
  } elseif ($sidebar_left) {
    $class = "boxLeft";
  } elseif ($sidebar_right) {
    $class = "boxRight";
  } else {
    $class = "box";
  }
  return $class;
}

?>