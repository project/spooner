<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
  
  <!--[if IE 6]><style>
  #boxLeft, #boxBoth { zoom: 1; }
  </style><![endif]-->
  
</head>

<body>
  <div id="head" class="clear-block">
    <div id="logo" class="clear-block">
      <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>">
      <?php 
      if ($logo) {?>
      <img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" />
      <?php } elseif ($site_name) {?>
      <h1 id="site-name">
        <?php print $site_name; ?>
      </h1>
      <?php } ?>
      </a>
    </div>
    <?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?><?php } ?>
      
  </div>
  <?php if ($search_box): ?><div id="search"><?php print $search_box ?></div><?php endif; ?>
  <div id="shadow"></div>

<div id="container">
<div id="<?php print $boxClass ?>">
<div id="center" class="column">

  <div id="mainBox">
    <?php if ($breadcrumb) { ?>
    <div id="breadcrumbs">
          <?php print $breadcrumb ?>
    </div>
    <?php } ?>

    <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      
          <h1 class="title"><?php print $title ?></h1>
          <div class="tabs"><?php print $tabs ?></div>
          <?php print $help ?>
          <?php print $messages ?>
          <?php print $content; ?>
          <?php print $feed_icons; ?>
          <br style="clear:both" />
    
  </div>

</div>

<?php if ($sidebar_left) { ?>
<div id="left" class="column">
  <div id="sidebar_left">
     <?php print $sidebar_left ?>
   </div>
</div>
<?php } ?>
<?php if ($sidebar_right) { ?>
<div id="right" class="column">
  <div id="sidebar_right">
     <?php print $sidebar_right ?>
   </div>
</div>
<?php } ?>

<?php if (($sidebar_left) || ($sidebar_right)) { ?>
<br style="clear:both">
<?php } ?>

</div>
</div>


<div id="footer">
  <?php print $footer_message ?>
    <?php if (isset($secondary_links)) { ?><?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist')) ?><?php } ?>
</div>
<?php print $closure ?>
</body>
</html>